
resource "aws_instance" "instance" {
    for_each = var.instances
    ami = each.value.image_id
    instance_type = each.value.instance_type
    vpc_security_group_ids = each.value.sg_id
    key_name = each.value.key_name
    subnet_id = each.value.subnet_id
    tags = {
        Name = each.key
        Env = var.env
    }
}