variable "sg_configuration" {
 #   type = map(map)
    default = { 
        sg-ssh = {port = 22, cidr = "0.0.0.0/0"}, 
        sg-http = {port = 80, cidr = "0.0.0.0/0"}, 
        sg-tomcat = {port = 8080, cidr = "10.10.0.0/16"}, 
        sg-rds = {port = 3306, cidr = "0.0.0.0/0" } 
    }
}

variable "project" {}
variable "vpc_cidr" {}
variable "region" {}
variable "environment" {}
variable "pri_subnet_cidr" {}
variable "pub_subnet_cidr" {}
variable "pub_subnet_az" {}
variable "pri_subnet_az" {}

/*
variable "instances {
    default = {
        jump_server = {image_id = "", instance_type = "t2.micro", sg_id = "", key_name = "", subnet_id = ""},
        tomcat_server = {image_id = "", instance_type = "t2.micro", sg_id = "", key_name = "", subnet_id = ""},
        proxy_server = {image_id = "", instance_type = "t2.micro", sg_id = "", key_name = "", subnet_id = ""},
    }
}
*/

variable "tomcat_server_ami" {}
variable "proxy_server_ami" {}
variable "jump_server_ami" {}
variable "tomcat_instance_type" {}
variable "jump_instance_type" {}
variable "proxy_instance_type" {}
variable "ssh_key" {}